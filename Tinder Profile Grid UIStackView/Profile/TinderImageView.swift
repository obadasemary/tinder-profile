//
//  TinderImageView.swift
//  Tinder Profile Grid UIStackView
//
//  Created by Abdelrahman Mohamed on 3/19/18.
//  Copyright © 2018 Abdelrahman Mohamed. All rights reserved.
//

import UIKit

class TinderImageView: UIImageView {
    
    let imageIndexLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 20)
        
        label.layer.shadowOpacity = 0.7
        label.layer.shadowOffset = .zero
        
        return label
    }()
    
    @IBInspectable
    var imageIndex: NSNumber! {
        didSet {
            let imageName = "daenerys\(imageIndex.stringValue)"
            self.image = UIImage(named: imageName)
            
            layer.cornerRadius = 10
            imageIndexLabel.text = imageIndex.stringValue
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addSubview(imageIndexLabel)
        imageIndexLabel.translatesAutoresizingMaskIntoConstraints = false
        imageIndexLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        imageIndexLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
    }
}
